package noobbot;

import com.google.gson.Gson;
import noobbot.carpositions.CarId;
import noobbot.carpositions.CarPosition;
import noobbot.carpositions.CarPositions;
import noobbot.engine.ThrottleCalculator;
import noobbot.engine.ThrottleCalculatorByAlex;
import noobbot.gameinit.Car;
import noobbot.gameinit.GameInit;
import noobbot.messages.*;

import java.io.*;
import java.net.Socket;

public class Main {
    final Gson gson = new Gson();
    private PrintWriter writer;

    private Car ourCar = null;
    private CarId ourCarId = null;
    private GameInit gameInit = null;

    // Create your custom implementation
    private ThrottleCalculator throttleCalculator = new ThrottleCalculatorByAlex();

    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
    }

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;
        send(join);
        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
                processCarPositionsMsg(line);
            } else if (msgFromServer.msgType.equals("join")) {
                printMsgFromServer(msgFromServer);
            } else if (msgFromServer.msgType.equals("yourCar")) {
                printMsgFromServer(msgFromServer);
                processYourCarMsg(msgFromServer);
            } else if (msgFromServer.msgType.equals("gameInit")) {
                processGameInitMsg(msgFromServer);
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                printMsgFromServer(msgFromServer);
            } else if (msgFromServer.msgType.equals("gameStart")) {
                printMsgFromServer(msgFromServer);
            } else if (msgFromServer.msgType.equals("tournamentEnd")) {
                printMsgFromServer(msgFromServer);
            } else if (msgFromServer.msgType.equals("crash")) {
                processCrashMsg(msgFromServer);
            } else if (msgFromServer.msgType.equals("spawn")) {
                printMsgFromServer(msgFromServer);
            } else if (msgFromServer.msgType.equals("lapFinished")) {
                printMsgFromServer(msgFromServer);
            } else if (msgFromServer.msgType.equals("dnf")) {
                printMsgFromServer(msgFromServer);
            } else if (msgFromServer.msgType.equals("finish")) {
                printMsgFromServer(msgFromServer);
            } else {
                send(new Ping());
            }
        }
        System.out.println("Final message: " + line);
    }

    private void processCarPositionsMsg(String line) {
        final CarPositions carPositions = gson.fromJson(line, CarPositions.class);
        CarPosition ourPosition = carPositions.ourCarPosition(ourCar.id);
        double throttle = throttleCalculator.getThrottle(gameInit, carPositions, ourPosition, ourCar);
        System.out.println("Throttle: " + throttle);
        send(new Throttle(throttle));
    }

    private void processYourCarMsg(MsgWrapper msgFromServer) {
        ourCarId = gson.fromJson(msgFromServer.data.toString(), CarId.class);
        System.out.println("OUR CAR: " + ourCarId.toString());
    }

    private void processGameInitMsg(MsgWrapper msgFromServer) {
        printMsgFromServer(msgFromServer);
        gameInit = gson.fromJson(msgFromServer.data.toString(), GameInit.class);
        ourCar = gameInit.race.getByCarId(ourCarId);

    }

    private void processCrashMsg(MsgWrapper msgFromServer) {
        printMsgFromServer(msgFromServer);
    }

    private void sendThrottle(double throttle) {
        send(new Throttle(throttle));
        System.out.println(throttle);
    }

    private void sendSwitchLane(String direction) {
        if (!direction.isEmpty()) {
            System.out.println("Switching lane to " + direction);
            send(new SwitchLane(direction));
        }
    }

    private void printMsgFromServer(MsgWrapper msg) {
        String msgString = "type: " + msg.msgType + "; data: " + msg.data;
        System.out.println(msgString);
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}





