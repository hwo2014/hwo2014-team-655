package noobbot.engine;

import noobbot.carpositions.CarPosition;
import noobbot.carpositions.CarPositions;
import noobbot.gameinit.Car;
import noobbot.gameinit.GameInit;

public interface ThrottleCalculator {
    public double getThrottle(GameInit gameInit, CarPositions carPositions, CarPosition ourPosition, Car ourCar);
}
