package noobbot.engine;

import noobbot.DefaultValues;
import noobbot.carpositions.CarPosition;
import noobbot.carpositions.CarPositions;
import noobbot.gameinit.Car;
import noobbot.gameinit.GameInit;
import noobbot.gameinit.Piece;
import noobbot.gameinit.Track;

public class ThrottleCalculatorByAlex implements ThrottleCalculator {
    private double instantSpeed = -1;
    private CarPosition prevPosition;

    @Override
    public double getThrottle(GameInit gameInit, CarPositions carPositions, CarPosition ourPosition, Car ourCar) {
        double throttle = 0.2;
        int actualPieceIndex = ourPosition.piecePosition.pieceIndex;
        Piece actualPiece = gameInit.race.track.pieces.get(actualPieceIndex);
        double prevInstantSpeed = instantSpeed;
        instantSpeed = getInstantSpeed(gameInit.race.track, ourPosition);
        // TODO should I switch lane? (even in bends?)
        if (Math.abs(actualPiece.angle) < DefaultValues.ANGLE_LIMIT) {
            // All straight pieces and bends with small angles
            throttle = 1.0;
            int piecesUntilNextBend = gameInit.race.track.piecesBeforeNextBend(actualPieceIndex);
            if (prevInstantSpeed > 0 && piecesUntilNextBend < 1) {
                double distanceToBend = gameInit.race.track.getDistanceBetweenPieces(actualPieceIndex,
                        ourPosition.piecePosition,
                        actualPieceIndex + piecesUntilNextBend);
                // Calculate ticks
                double nextBendAngle = gameInit.race.track.pieces.get(actualPieceIndex + piecesUntilNextBend).angle;
                int ticksToBend = (int) (((int) distanceToBend) / instantSpeed);
                double decrement = Math.min(getThrottleDecrement(ticksToBend, nextBendAngle), 1.0);
                throttle -= decrement;
            }
        } else {
            // Fucking bends!!
            throttle = 0.2;
        }
        prevPosition = ourPosition;
        System.out.println(String.format("", instantSpeed, throttle));
        return throttle;
    }

    private double getThrottleDecrement(int ticksToBend, double bendAngle) {
        // TODO magic formula
        double result = (instantSpeed/10) / ticksToBend;
        result = result * bendAngle / DefaultValues.MAX_ANGLE;
        return result;
    }

    private double getInstantSpeed(Track track, CarPosition ourPosition) {
        if (prevPosition != null) {
            instantSpeed = getDistanceBetweenCarPositions(track, prevPosition, ourPosition);
        }
        return instantSpeed;
    }

    private double getDistanceBetweenCarPositions(Track track, CarPosition start, CarPosition end) {
        double distance;
        int prevPieceIndex = start.piecePosition.pieceIndex;
        int actualPieceIndex = end.piecePosition.pieceIndex;
        // TODO add laneDistanceFromCenter
        if (prevPieceIndex == actualPieceIndex) { // Same piece
            distance = end.piecePosition.inPieceDistance - start.piecePosition.inPieceDistance;
        } else {
            double inPieceDistanceLeft = track.pieces.get(prevPieceIndex).getLength(0) - start.piecePosition.inPieceDistance;
            distance = end.piecePosition.inPieceDistance + inPieceDistanceLeft;
        }
        return distance;
    }
}
