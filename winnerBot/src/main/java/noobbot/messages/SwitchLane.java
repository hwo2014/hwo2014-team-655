package noobbot.messages;

public class SwitchLane extends SendMsg {

    private String value;

    public SwitchLane(String value) {
        this.value = value;
    }

    @Override
    public String toJson() {
        return super.toJson();
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}
