package noobbot.gameinit;

public class GameInit {

    public Race race;

    @Override
    public String toString() {
        return "GAME INIT: race: " + race;
    }

}
