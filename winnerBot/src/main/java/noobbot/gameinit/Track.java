package noobbot.gameinit;

import noobbot.DefaultValues;
import noobbot.carpositions.PiecePosition;

import java.util.List;

public class Track {

    public String id;
    public String name;
    public List<Piece> pieces;
    public List<Lane> lanes;
    public StartingPoint startingPoint;

    public double getDistanceBetweenPieces(int firstPiece, PiecePosition pos, int lastPiece) {
        double distance = 0;
        for (Piece piece : pieces.subList(firstPiece, lastPiece)) {
            distance += piece.getLength(lanes.get(pos.lane.startLaneIndex).distanceFromCenter);
        }
        return distance;
    }

    public int piecesBeforeNextBend(int actualPieceIndex) {
        for (int i = actualPieceIndex, n = pieces.size(); i < n; ++i) {
            if (pieces.get(i).angle > DefaultValues.ANGLE_LIMIT) {
                return i;
            }
        }
        return 30; // No bends? :D
    }
}
