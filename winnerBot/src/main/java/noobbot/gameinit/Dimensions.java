package noobbot.gameinit;

public class Dimensions {

    public double length;
    public double width;
    public double guideFlagPosition;

    // 0.5 means in the middle
    public double getGuideFlagPos() {
        return guideFlagPosition/length;
    }
}
