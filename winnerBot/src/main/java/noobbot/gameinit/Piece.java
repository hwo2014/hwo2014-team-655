package noobbot.gameinit;

import com.google.gson.annotations.SerializedName;

public class Piece {

    public double length;
    @SerializedName("switch")
    public boolean switchFlag;
    public int radius;
    public double angle;

    // laneDFC = lane distance from center
    public double getLength(int laneDFC) {
        if (angle != 0) {
            length = 2 * Math.PI * (radius + laneDFC) * angle / 360;
        }
        return length;
    }
}
