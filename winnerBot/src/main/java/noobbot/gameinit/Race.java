package noobbot.gameinit;

import noobbot.carpositions.CarId;

import java.util.List;

public class Race {

    public Track track;
    public List<Car> cars;
    public RaceSession raceSession;

    public Car getByCarId(CarId ourCarId) {
        for (Car car : cars) {
            if (car.id.color.equals(ourCarId.color)
                    && car.id.name.equals(ourCarId.name)) {
                return car;
            }
        }
        throw new RuntimeException("There's no car matching name " + ourCarId.name
                + " and color " + ourCarId.color);
    }
}
