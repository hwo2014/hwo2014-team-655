package noobbot;

public final class DefaultValues {
    public static final double ANGLE_LIMIT = 15;
    public static final double MAX_ANGLE = 45;
}
