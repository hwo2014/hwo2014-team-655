package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import com.google.gson.Gson;
import noobbot.carpositions.CarPositions;
import noobbot.gameinit.GameInit;
import noobbot.switchLane.SwitchLane;
import noobbot.switchLane.ViktorSwitchLaneAction;
import noobbot.throttle.ViktorThrottleAction;
import noobbot.yourcar.YourCar;
import noobbot.throttle.Throttle;

public class Main {

    //TODO move to config file
    public static final String RACE = "germany"; //"keimola", "germany"
    public static final int CAR_COUNT = 1;

    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new JoinRace(botName, botKey, RACE, CAR_COUNT));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    ViktorThrottleAction throttleAction = new ViktorThrottleAction();
    ViktorSwitchLaneAction switchLaneAction = new ViktorSwitchLaneAction();

    private YourCar ourCar = null;
    private GameInit gameInit = null;


    public Main(final BufferedReader reader, final PrintWriter writer, final JoinRace joinRace) throws IOException {
        this.writer = writer;
        String line = null;

        send(joinRace);

        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
                processCarPositionsMsg(line);
            } else if (msgFromServer.msgType.equals("join")) {
                printMsgFromServer(msgFromServer);
            } else if (msgFromServer.msgType.equals("yourCar")) {
                processYourCarMsg(line);
            } else if (msgFromServer.msgType.equals("gameInit")) {
                processGameInitMsg(msgFromServer);
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                printMsgFromServer(msgFromServer);
            } else if (msgFromServer.msgType.equals("gameStart")) {
                printMsgFromServer(msgFromServer);
            } else if (msgFromServer.msgType.equals("tournamentEnd")) {
                printMsgFromServer(msgFromServer);
            } else if (msgFromServer.msgType.equals("crash")) {
                processCrashMsg(msgFromServer);
            } else if (msgFromServer.msgType.equals("spawn")) {
                printMsgFromServer(msgFromServer);
            } else if (msgFromServer.msgType.equals("lapFinished")) {
                printMsgFromServer(msgFromServer);
            } else if (msgFromServer.msgType.equals("dnf")) {
                printMsgFromServer(msgFromServer);
            } else if (msgFromServer.msgType.equals("finish")) {
                printMsgFromServer(msgFromServer);
            } else {
                send(new Ping());
            }
        }
        System.out.println("Final message: " + line);
    }

    private void processCarPositionsMsg(String line) {
        final CarPositions carPositions = gson.fromJson(line, CarPositions.class);
        String switchLaneDirection = switchLaneAction.getSwitchLaneDirection(gameInit, carPositions, ourCar);
        sendSwitchLane(switchLaneDirection);
        double throttle = throttleAction.getThrottleByAngleProximity(gameInit, carPositions, ourCar);
        sendThrottle(throttle);
    }

    private void processYourCarMsg(String line) {
        ourCar = gson.fromJson(line, YourCar.class);
        System.out.println("OUR CAR: " + ourCar.data.toString());
    }

    private void processGameInitMsg(MsgWrapper msgFromServer) {
        printMsgFromServer(msgFromServer);
        gameInit = gson.fromJson(msgFromServer.data.toString(), GameInit.class);
    }

    private void processCrashMsg(MsgWrapper msgFromServer) {
        printMsgFromServer(msgFromServer);
        throttleAction.throttle = 0.6;
    }

    private double throttle = -1;
    private void sendThrottle(double throttle) {
        if (this.throttle != throttle) {
            this.throttle = throttle;
        }
        send(new Throttle(this.throttle));
    }

    private void sendSwitchLane(String direction) {
        if (!direction.isEmpty()) {
            System.out.println("Switching lane to " + direction);
            send(new SwitchLane(direction));
        }
    }

    private void printMsgFromServer(MsgWrapper msg) {
        String msgString = "type: " + msg.msgType + "; data: " + msg.data;
        System.out.println(msgString);
    }

    private void send(final SendMsg msg) {
        //System.out.println("Sending: " + msg.toJson());
        writer.println(msg.toJson());
        writer.flush();
    }
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class JoinRace extends SendMsg {

    class BotId {
        public String name;
        public String key;

        public BotId(String name, String key) {
            this.name = name;
            this.key = key;
        }
    }

    public Object botId;
    public String trackName;
    public int carCount;

    public JoinRace(String name, String key, String trackName, int carCount) {
        botId = new BotId(name, key);
        this.trackName = trackName;
        this.carCount = carCount;
    }

    @Override
    protected String msgType() {
        return "joinRace";
    }

}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

