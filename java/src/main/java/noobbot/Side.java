package noobbot;

public enum Side {

    LEFT("Left"), RIGHT("Right"), NONE("None");

    private String name;
    public String getName() {
        return name;
    }

    Side(String name) {
        this.name = name;
    }

    public static Side getSide(double numericSide) {
        if (numericSide < 0) {
            return Side.LEFT;
        } else if (numericSide > 0) {
            return Side.RIGHT;
        } else {
            return Side.NONE;
        }
    }

}
