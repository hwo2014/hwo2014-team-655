package noobbot.gameinit;

import noobbot.Side;

public class Lane {

    public int distanceFromCenter;
    public int index;

    public Side getSide() {
        return Side.getSide(distanceFromCenter);
    }

}
