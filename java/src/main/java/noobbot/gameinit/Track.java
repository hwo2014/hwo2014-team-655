package noobbot.gameinit;

import noobbot.Side;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Track {

    public String id;
    public String name;
    public List<Piece> pieces;
    public List<Lane> lanes;
    public StartingPoint startingPoint;

    // TODO Cache data needed by methods until the change of currentPieceIndex

    public double getDistanceUntilNextCurve(int currentPieceIndex, double inPieceDistance) {
        double distance = pieces.get(currentPieceIndex).length - inPieceDistance;
        for (Piece piece : getPiecesUntilNextCurve(currentPieceIndex, false)) {
            distance += piece.length;
        }
        return distance;
    }

    public Piece getPieceWithNextCurve(int currentPieceIndex) {
        List<Piece> list = getPiecesUntilNextCurve(currentPieceIndex, true);
        return list.get(list.size() - 1);
    }

    // TODO Figure out how to calculate distance instead of using pieces as unit
    public double getDistanceLeftInCurve(int currentPieceIndex) {
        List<Piece> list = getPiecesUntilTheEndOfCurve(currentPieceIndex);
        double distance = 0;
        for (Piece piece : list) {
            distance += 1;
        }
        return distance;
    }

    public int getNextSwitchPieceIndex(int currentPieceIndex, int skipSwitches) {
        int startIndex = currentPieceIndex + 1;
        int endIndex = pieces.size() + currentPieceIndex;
        for (int pieceIndex = startIndex; pieceIndex < endIndex; pieceIndex++) {
            Piece piece = pieces.get(pieceIndex % pieces.size());
            if (piece.switchFlag) {
                if (skipSwitches == 0) {
                    return pieceIndex % pieces.size();
                } else {
                    skipSwitches--;
                }
            }
        }
        return -1;
    }

    public List<Integer> getCurvesBetweenPieceIndexes(int startPieceIndex, int endPieceIndex) {
        List<Integer> list = new ArrayList<>();
        int startIndex = startPieceIndex + 1;
        int endIndex;
        if (endPieceIndex > startPieceIndex) {
            endIndex = endPieceIndex;
        } else {
            endIndex = endPieceIndex + pieces.size();
        }
        for (int index = startIndex; index <= endIndex; index++) {
            Piece piece = pieces.get(index % pieces.size());
            if (piece.angle != 0) {
                list.add(index % pieces.size());
            }
        }
        return list;
    }

    public Side getLongestCurveSide(List<Integer> piecesIndexes) {
        int left = 0;
        int right = 0;
        for (int pieceIndex : piecesIndexes) {
            Piece piece = pieces.get(pieceIndex);
            Side side = Side.getSide(piece.angle);
            if (piece.angle > 0) {
                right++;
            } else if (piece.angle < 0) {
                left++;
            }
        }
        if (left > right) {
            return Side.LEFT;
        } else if (right > left) {
            return Side.RIGHT;
        } else {
            return Side.NONE;
        }
    }

    private List<Piece> getPiecesUntilNextCurve(int currentPieceIndex, boolean includeCurvedPiece) {
        List<Piece> list = new ArrayList<>();
        for(int index = 0; index < pieces.size(); index++) {
            int pieceIndex = index + currentPieceIndex + 1;
            if (pieceIndex >= pieces.size()) {
                pieceIndex = pieceIndex - pieces.size();
            }
            Piece piece = pieces.get(pieceIndex);
            if (piece.angle != 0) {
                if (includeCurvedPiece) {
                    list.add(piece);
                }
                break;
            } else {
                list.add(piece);
            }
        }
        return list;
    }

    private List<Piece> getPiecesUntilTheEndOfCurve(int currentPieceIndex) {
        List<Piece> list = new ArrayList<>();
        for(int index = 0; index < pieces.size(); index++) {
            int pieceIndex = index + currentPieceIndex + 1;
            if (pieceIndex >= pieces.size()) {
                pieceIndex = pieceIndex - pieces.size();
            }
            Piece piece = pieces.get(pieceIndex);
            if (piece.angle == 0) {
                break;
            } else {
                list.add(piece);
            }
        }
        return list;
    }

    public Piece getPiece(int currentPieceIndex) {
        return pieces.get(currentPieceIndex);
    }

    Map<Integer, Lane> laneMap = null;
    public Lane getLane(int laneIndex) {
        if (laneMap == null) {
            laneMap = new HashMap<>();
            for(Lane lane : lanes) {
                laneMap.put(lane.index, lane);
            }
        }
        return laneMap.get(laneIndex);
    }

}
