package noobbot.gameinit;

import com.google.gson.annotations.SerializedName;
import noobbot.Side;

public class Piece {

    public double length;
    @SerializedName("switch")
    public boolean switchFlag;
    public int radius;
    public double angle;

    public Side getSide() {
        return Side.getSide(angle);
    }

    public double getMaxSpeed(double carAngle, double prevCarAngle, double distanceLeftInCurve, double curvePieceDistanceLeft) {
        boolean stabilized = false;
        if (Math.abs(angle) < 45) {
            return 1.0;
        } else if (angle < 0 && carAngle > (prevCarAngle - 0.40) && carAngle < 0) {
            return 1.0;
        } else if (angle > 0 && carAngle < (prevCarAngle + 0.40) && carAngle > 0) {
            return 1.0;
//        } else if (distanceLeftInCurve == 0 && curvePieceDistanceLeft < 90) {
//        } else if (distanceLeftInCurve == 0) {
//            return 1.0;
        }
        return 0.65;
    }

}
