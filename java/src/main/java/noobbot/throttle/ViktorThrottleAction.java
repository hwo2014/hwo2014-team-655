package noobbot.throttle;

import noobbot.carpositions.CarPosition;
import noobbot.carpositions.CarPositions;
import noobbot.gameinit.GameInit;
import noobbot.gameinit.Piece;
import noobbot.gameinit.Track;
import noobbot.yourcar.YourCar;

public class ViktorThrottleAction {

    // TODO Calculate the exact length of a circle piece
    private final double CURVE_PIECE_DISTANCE = 100;

    private int currentPieceIndex = -1;
    public double throttle = -1;
    private int gameTick = -1;
    private double inPieceDistance = -1;
    private CarPosition carPosition;
    private double prevCarAngle;
    private double prevSpeed;
    private double curvePieceDistanceLeft;
    private double prevDistanceLeftInCurve = -1;

    public double getThrottleByAngleProximity(GameInit gameInit, CarPositions carPositions, YourCar ourCar) {
        if (gameTick != carPositions.gameTick) {
            carPosition = carPositions.ourCarPosition(ourCar.data.name, ourCar.data.color);
            currentPieceIndex = carPosition.piecePosition.pieceIndex;
            Track track = gameInit.race.track;
            Piece currentPiece = track.getPiece(currentPieceIndex);
            double distanceUntilNextAngle = track.getDistanceUntilNextCurve(currentPieceIndex, inPieceDistance);
            double speed = getSpeed();
            double nextCurveAngle = track.getPieceWithNextCurve(currentPieceIndex).angle;
            if (speed > 10) {
                speed = prevSpeed;
            }

            if (currentPiece.angle != 0) {
                double distanceLeftInCurve = track.getDistanceLeftInCurve(currentPieceIndex);
                if (distanceLeftInCurve != prevDistanceLeftInCurve) {
                    curvePieceDistanceLeft = CURVE_PIECE_DISTANCE;
                }
                curvePieceDistanceLeft -= speed;
                throttle = currentPiece.getMaxSpeed(carPosition.angle,
                        prevCarAngle,
                        distanceLeftInCurve,
                        curvePieceDistanceLeft);
                System.out.println("Curve: " + throttle + " " + speed + " " + distanceLeftInCurve + " " + curvePieceDistanceLeft);
                prevDistanceLeftInCurve = distanceLeftInCurve;
            // TODO Calculate when to slow down in order to reach certain point and speed
            } else if (Math.abs(nextCurveAngle) < 45) {
                throttle = 1.0;
                System.out.println("Before small curve");
            } else if (distanceUntilNextAngle <= 150 && speed > 8) {
                throttle = 0.0;
                System.out.println("Before curve");
            } else if (distanceUntilNextAngle <= 100 && speed > 6.5) {
                throttle = 0.0;
                System.out.println("Before curve");
            } else if (distanceUntilNextAngle <= 100 && speed < 6.0) {
                throttle = 1.0;
                System.out.println("Before curve");
            } else if (distanceUntilNextAngle <= 100) {
                throttle = 0.65;
                System.out.println("Before curve");
            } else {
                throttle = 1.0;
                System.out.println("Straight");
            }
            System.out.println(throttle + " " + speed);
            System.out.println(prevCarAngle + " " + carPosition.angle);
            gameTick = carPositions.gameTick;
            inPieceDistance = carPosition.piecePosition.inPieceDistance;
            prevCarAngle = carPosition.angle;
            prevSpeed = speed;
//            log(carPositions, speed, distanceUntilNextAngle, currentPiece);
        }
        return throttle;
    }

    public double getSpeed() {
        double speed = carPosition.piecePosition.inPieceDistance - inPieceDistance;
        if (speed < 0) {
            // TODO Remove hard-coded 100
            speed += 100;
        }
        return speed;
    }

    private void log(CarPositions carPositions, double speed, double distanceUntilNextAngle, Piece currentPiece) {
        if (gameTick % 30 == 0) {
            System.out.println("tick: " + carPositions.gameTick);
            System.out.println("inPieceDistance: " + carPosition.piecePosition.inPieceDistance);
            System.out.println("speed: " + speed);
            System.out.println("getDistanceUntilNextCurve: " + distanceUntilNextAngle);
            System.out.println("throttle: " + throttle);
            if (distanceUntilNextAngle <= 0) {
                System.out.println("angle: " + currentPiece.angle + " " + Math.abs(currentPiece.angle));
            }
        }
    }

}
