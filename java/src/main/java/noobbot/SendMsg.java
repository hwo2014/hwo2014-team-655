package noobbot;

import com.google.gson.Gson;

/**
 * Created by Viktor on 4/16/2014.
 */
public abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}
