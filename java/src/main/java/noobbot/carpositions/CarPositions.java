package noobbot.carpositions;

import java.util.List;

public class CarPositions {

    public List<CarPosition> data;
    public String gameId;
    public int gameTick;

    public CarPosition ourCarPosition(String name, String color) {
        for (CarPosition carPosition : data) {
            //Search for the car with the same name and color (A car is identified by {name, color})
            if (carPosition.id.name.equals(name) && carPosition.id.color.equals(color)) {
                return carPosition;
            }
        }
        throw new RuntimeException("Could not find car position for " + name + " (" + color + ")");
    }

}
