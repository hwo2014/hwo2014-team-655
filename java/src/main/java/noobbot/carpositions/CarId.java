package noobbot.carpositions;

public class CarId {

    public String name;
    public String color;

    @Override
    public String toString() {
        return "name: " + name + "; color: " + color;
    }

}
