package noobbot.switchLane;

import noobbot.Side;
import noobbot.carpositions.CarPosition;
import noobbot.carpositions.CarPositions;
import noobbot.gameinit.GameInit;
import noobbot.gameinit.Piece;
import noobbot.gameinit.Track;
import noobbot.yourcar.YourCar;

import java.util.List;

public class ViktorSwitchLaneAction {

    boolean waitingSwitch = false;

    public String getSwitchLaneDirection(GameInit gameInit, CarPositions carPositions, YourCar ourCar) {
        CarPosition carPosition = carPositions.ourCarPosition(ourCar.data.name, ourCar.data.color);
        int currentPieceIndex = carPosition.piecePosition.pieceIndex;
        Track track = gameInit.race.track;
        Piece currentPiece = track.getPiece(currentPieceIndex);
        if (currentPiece.switchFlag) {
            waitingSwitch = false;
        } else if (!waitingSwitch) {
            int firstSwitchPieceIndex = track.getNextSwitchPieceIndex(currentPieceIndex, 0);
            if (firstSwitchPieceIndex == currentPieceIndex + 1) {
                int secondSwitchPieceIndex = track.getNextSwitchPieceIndex(currentPieceIndex, 1);
                List<Integer> curvesBetweenPieceIndexes =
                        track.getCurvesBetweenPieceIndexes(firstSwitchPieceIndex, secondSwitchPieceIndex);
                Side curveSide = track.getLongestCurveSide(curvesBetweenPieceIndexes);
                int laneIndex = carPositions.ourCarPosition(ourCar.data.name, ourCar.data.color)
                        .piecePosition.lane.startLaneIndex;
                Side laneSide = track.getLane(laneIndex).getSide();
                waitingSwitch = true;
                if (laneSide != curveSide && curveSide != Side.NONE) {
                    return curveSide.getName();
                } else {
                    System.out.println("NOT switching lane");
                }
            }
        }
        return "";
    }

    public void setLaneSwitched() {
        waitingSwitch = false;
    }

}
