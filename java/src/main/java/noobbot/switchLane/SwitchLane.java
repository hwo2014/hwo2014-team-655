package noobbot.switchLane;

import noobbot.SendMsg;

public class SwitchLane extends SendMsg {

    private String value;

    public SwitchLane(String value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}
